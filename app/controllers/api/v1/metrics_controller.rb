class Api::V1::MetricsController < ApplicationController
    
    def index
        @metrics = Metric.order('name')
        render json: @metrics
    end
    
    def getMetricGraphData
        resultados = {}
       
        if params[:metric].present? && params[:metric].to_s.downcase == "all"
            @metrics = Metric.all
        else
            @metrics = Metric.where(name: params[:metric].downcase)
        end

        if @metrics.blank?
            render json: {"status": "NotOk", "msg":"no encontre metrica " + params[:metric].downcase}, status: 400
        else
            @metrics.each do |m|
                #To check
                #MetricPoint.select("avg(value) as value, FROM_UNIXTIME((UNIX_TIMESTAMP(created_at) div 900) * 900) as timestamp, metric_id")
                #.where("metric_id= :metric_id AND created_at >= :start_date AND created_at <= :end_date",{:metric_id => m.id.to_s, :start_date => params[:fromDate], :end_date => params[:toDate]})
                #.group("timestamp")
                sql = "SELECT avg(value) as value, metric_id, "
                sql = sql + "FROM_UNIXTIME((UNIX_TIMESTAMP(created_at) div " + params[:avgWindow].to_s + ") * " + params[:avgWindow].to_s + ") as timestamp "
                sql = sql + "FROM metric_points "
                sql = sql + " where metric_id = " + m.id.to_s
                sql = sql + " AND date(created_at) BETWEEN '" + params[:fromDate].to_s + "' AND '" + params[:toDate].to_s + "' "
                sql = sql + " GROUP BY timestamp"
   
                results = ActiveRecord::Base.connection.exec_query(sql)
        
                if results.present?
                    resultados[m.name] = results 
                end
            end
            render json: resultados
        end
        
    end

    def create
        if params[:name].present?
        #if metric_params.permitted?
            @metric = Metric.find_by_name(params[:name].downcase)
            if @metric.blank?
                @metric = Metric.new
                @metric.name = params[:name].downcase
                if !@metric.save
                    render json: {"status": "NotOk", "msg":"Something went wrong when creating a new Metric!", "errors": @metric.errors.details}, status: 400                     
                end
            end
            @point = MetricPoint.new
            @point.metric = @metric
            @point.value = params[:value]
            @point.save
            render json: @point 
        else
            render json: {"status": "NotOk", "msg":"Parameter name and value are required"}, status: 400 
        end
    end
 
    private
    def metric_params
        params.require(:metric).permit(:name, :value)
    end
end
