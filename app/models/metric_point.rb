class MetricPoint < ApplicationRecord
  validates :value, numericality: true, presence: true
  
  belongs_to :metric
end
