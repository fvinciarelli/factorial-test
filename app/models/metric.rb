class Metric < ApplicationRecord
    validates :name, length: { minimum: 1 }, uniqueness: { message: "%{value} seems wrong" }, presence: { message: "%{value} seems wrong" }
    
    has_many :metric_points
end
