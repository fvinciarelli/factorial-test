class CreateMetricPoints < ActiveRecord::Migration[5.2]
  def change
    create_table :metric_points do |t|
      t.string :value
      t.belongs_to :metric, foreign_key: true

      t.timestamps
    end
  end
end
