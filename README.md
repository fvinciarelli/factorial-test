# README

## How to config and use this project

## 🚀 Environment Setup

### 🐳 Needed tools

1. [Install Docker](https://www.docker.com/get-started)
2. In a terminal run the following commands:
3. Clone this project: `git clone https://gitlab.com/fvinciarelli/factorial-test.git vinciarelli-ruby`
4. Move to the project folder: `cd vinciarelli-ruby`

### 🛠️ Environment configuration and Application execution

Allways at the project's root directory run:

1. `docker-compose build` , to build the dockers images
2. `docker-compose up -d` , to start the dockers containers
3. `docker-compose run web sh -c 'cd bin && rake db:migrate'` , to set up app and testing databases

### 🔥 Application execution

The solution is an API project so there's no view. To check solution features you have to call the defined endpoints:

1. [Get Metrics List](http://localhost:3000/api/v1/metrics) or at command line using cURL:

`
curl -X GET \
  'localhost:3000/api/v1/metrics' \
  --header 'Accept: */*' \
  --header 'User-Agent: Thunder Client (https://www.thunderclient.com)' \
  --header 'Content-Type: application/json' \
}'
`

2. Use the following POST call to add a new metric point: (Ex. metric = 'First', value= '1.5')

`
curl -X POST \
  'localhost:3000/api/v1/metrics' \
  --header 'Accept: */*' \
  --header 'User-Agent: Thunder Client (https://www.thunderclient.com)' \
  --header 'Content-Type: application/json' \
  --data-raw '{
    "name" : "First",
    "value": "1.5"
}'
`

3. To Get an array of data points for all registered metrics every 15 minutes run the following command  using cURL:

`
curl -X GET \
  'http://localhost:3000/api/v1/graph_data?metric=all&fromDate=2022-01-01&toDate=2022-10-01&avgWindow=900' \
  --header 'Accept: */*' \
  --header 'User-Agent: Thunder Client (https://www.thunderclient.com)'
`

### ✅ Testing

In order to check is solution is passing test run the following command:

1. `docker-compose run web sh -c 'cd bin && rake test'` , to check that app is passing tests correctly
