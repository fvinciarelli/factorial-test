Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api do
    namespace :v1 do
      get 'metrics', to: 'metrics#index'
      post 'metrics', to: 'metrics#create'
      get 'graph_data', to: 'metrics#getMetricGraphData'
    end
  end
end
