require 'test_helper'

class MetricsControllerTest < ActionDispatch::IntegrationTest

  self.use_instantiated_fixtures = true

  test "can list registered metrics" do
    get "/api/v1/metrics", :as => "json"

    #Response has no errors
    assert_response :success

    #Should return 2 record encoded as JSON
    body = JSON.parse(@response.body)
    assert_equal 2, body.length

    #First Record should have 'Metrica 1' as name
    assert_equal "Metrica1", body[0]["name"]
  end

  test "can register a metric an its value" do

    assert_difference 'Metric.count' do
      post "/api/v1/metrics", as: :json, params: { name: "a Metric", value: 32 }
    end
    
    #Response has no errors
    assert_response :success

    #Should return metric point record encoded as JSON
    body = JSON.parse(@response.body)
    assert_equal 32, body['value'].to_i

  end

  test "can get graph data points to plot an specific metric in a timeline" do

    #Get the id for the metric
    get "/api/v1/graph_data", params: { metric: "All", fromDate: "2021-12-01", toDate: "2022-02-01", avgWindow: "900" }, as: "json"
    
    #Response has no errors
    assert_response :success

    #Should return metric point record encoded as JSON
    #body = JSON.parse(@response.body)
    #assert_equal 32, body

  end

end
